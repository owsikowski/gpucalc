


(import http-client (chicken io))
(import srfi-1)
(import srfi-13)
(import (chicken string))
(import ssax) 
(import sxpath)
(import html-parser)

(define (webpage-download-from-uri uri-path)
        (with-input-from-request uri-path #f read-string))



(import brev)

(define (webpage-download-from-uri uri-path)
        (with-input-from-request uri-path #f read-string))
(define whattomine-html (webpage-download-from-uri "https://www.whattomine.com/gpus?cost=0.16&button=&nvidia=true&amd=true&filter=all"))
 (define (przygotuj-string-dla-olx ss) (strse ss " " "-"))
(define lista-kart-graficznych (map przygotuj-string-dla-olx (remove empty? (map string-trim (-> whattomine-html html->sxml ((sxpath `(// td  a *text*))) ) ))))
(define (str-contains ss exp) (string-contains exp ss))

(define dollar-values (filter (c str-contains "$") (filter string? (-> whattomine-html html->sxml ((sxpath `(// td  ))) flatten ))))
(import list-utils)
(import strse)
(define (drop-first-char ss) (string-drop ss 1))
(define 24-gpu-incomes (map string->number (map drop-first-char (map string-trim-both (map second (section dollar-values 5))))))


(define (ask-olx-html karta)(webpage-download-from-uri (string-append "https://www.olx.pl/elektronika/komputery/podzespoly-i-czesci/q-" karta)))

(define (ceny-karty-stringi karta)  (filter (c str-contains " zł") (filter string? (-> karta ask-olx-html html->sxml flatten))))

(define (przygotuj-string-cen-karty ss)  (strse ss " " "" "zł" ""))
					; (map przygotuj-string-cen-karty (ceny-karty-stringi "rtx-3090"))

(define (ściągnij-ceny-karty ss) (remove empty? (map string->number (map przygotuj-string-cen-karty (ceny-karty-stringi ss)))) )
(import statistics)
(define (duże x) (> x 1000))
 (define (mediana* ll) (if (empty? ll) #f (+ (median ll) 0.0)))
(define (sprawdź-mediane-karty ss) (mediana* (filter duże (ściągnij-ceny-karty ss))))
(define średnie-ceny-kart (map sprawdź-mediane-karty lista-kart-graficznych))
(define kurs-dolara  (string->number (strse (przygotuj-string-cen-karty (first (filter (c str-contains " zł") ((sxpath `(// div *text*))(html->sxml (webpage-download-from-uri "https://www.bankier.pl/waluty/kursy-walut/nbp/USD")))))) "," ".")))
(define (dzielacz a b) (if (or (eq? a #f) (eq? b 0) (eq? b #f)) #f (+ (/ a b) 0.0 ) ))

					;już naobliczalismy się, czas coś zaprezentować
(define dzienny-zarobek-kart (map (c * kurs-dolara) 24-gpu-incomes))
(define czasy-zwrotu (map dzielacz średnie-ceny-kart dzienny-zarobek-kart))
(print czasy-zwrotu)
(print (length czasy-zwrotu))
(print (list? czasy-zwrotu))
(import list-comprehensions)
(define roczna-stopa-zwrotu (map dzielacz (repeat (length czasy-zwrotu) 36500) czasy-zwrotu))
;(define roczna-stopa-zwrotu (map dzielacz (repeat (length czasy-zwrotu) 36500)  (repeat (length czasy-zwrotu) 36500)))
(print roczna-stopa-zwrotu)
(define zestawienie-kart  (filter third (zip lista-kart-graficznych dzienny-zarobek-kart średnie-ceny-kart czasy-zwrotu)))

;(define (zaokrąglij liczba) (if (string? liczba) (if (string->number liczba) (/ (truncate (* 100 (string->number liczba))) 100) )) liczba)
(define (zaokrąglij liczba) (if (flonum? liczba) (/ (truncate (* 100 liczba)) 100) liczba)) 
 (define (dvv ss) `(td ,(zaokrąglij ss)))
(define (spp ll) `(tr ,ll))
 (define table1 (map spp (section (map dvv (flatten zestawienie-kart)) 4)))
(define export-html (sxml->html `(table (@ (id "gputable")) (thead (tr (th "karta") (th "dzienny zarobek") (th "cena") (th "czas zwrotu w dniach") (th "roczna stopa zwrotu w %")))  (tbody ,table1 ))))
(import (chicken file))
(delete-file "/var/www/html/statix/gpu.html")
(file-write  (file-open "/var/www/html/statix/gpu.html" (+ open/write open/creat)) export-html)

 (print "ready")
